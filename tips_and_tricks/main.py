import socket
import re

connection_tuple = ("f0a9de3514bd3607.247ctf.com", 50073)


def main():
    regex = re.compile(r"\s(\d+)\s\+\s(\d+)",
                       re.IGNORECASE | re.MULTILINE)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(connection_tuple)
    i = 0
    while True:
        data = s.recv(1024).decode('utf-8')
        lines = data.split('\r\n')
        lines.remove('')
        line = lines[-1]
        print(f'{i}) {line}')
        question = regex.search(line)
        a = int(question.group(1))
        b = int(question.group(2))
        sum = a + b
        print(sum)
        s.send(f'{sum}\r\n'.encode('utf-8'))
        i += 1
        if not data:
            break


if __name__ == '__main__':
    main()
