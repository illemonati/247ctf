package main

import (
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/imroc/req"
)

const baseURL = "https://ebd5da0815475d7c.247ctf.com/encrypt"

func main() {

	user := "impossible_flag_user"
	userHex := hex.EncodeToString([]byte(user))
	fmt.Println(userHex, len(userHex)/2)
	// blockSize := findBlockSize()
	// fmt.Printf("Blocksize is %d\n", blockSize)

	userPadded := strings.Repeat("AA", 32) + userHex
	r, _ := req.Get(baseURL, req.QueryParam{
		"user": userPadded,
	})
	res, _ := r.ToString()
	fmt.Println(res)
	userEnc := res[32*2:]
	fmt.Println(userEnc)
	r, _ = req.Get("https://ebd5da0815475d7c.247ctf.com/get_flag", req.QueryParam{
		"user": userEnc,
	})
	res, _ = r.ToString()
	fmt.Println(res)
}

func findBlockSize() int {
	size := int(0)
	for i := 0; i < 50; i++ {
		r, err := req.Get(baseURL, req.QueryParam{
			"user": strings.Repeat("AA", i),
		})
		if err != nil {
			continue
		}
		res, err := r.ToString()
		fmt.Println(i, ")", res, len(res))
		if size == 0 {
			size = len(res)
			continue
		}
		if size != len(res) {
			return len(res) - size
		}
	}
	return 0
}
