const a = [1, 2, 3];

function* coolIter(arr) {
    for (const [key, val] of arr.entries()) {
        const obj = {};
        obj["key"] = key;
        obj["value"] = val;
        yield obj;
    }
    return;
}

const coolIterFunctional = (arr) =>
    arr.map((key, val) => {
        const obj = {};
        obj["key"] = key;
        obj["value"] = val;
        return obj;
    });

for (const { key: index, value: val } of coolIter(a)) {
    console.log(index, val);
}

for (const { key: index, value: val } of coolIterFunctional(a)) {
    console.log(index, val);
}

// one line
for (const { k: index, v: val } of a.map((v, k) => {
    return { k, v };
})) {
    console.log(index, val);
}
