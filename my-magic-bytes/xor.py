from itertools import cycle


def xor(message_str: str, key_str: str):
    key_bytes = bytes.fromhex(key_str.strip())
    message_bytes = bytes.fromhex(message_str.strip())
    # print(key_bytes)
    # print(message_bytes)
    xored_str = ''.join([hex((a ^ b))[2:].upper().rjust(2, '0') + ' '
                         for a, b in zip(message_bytes, cycle(key_bytes))])
    return xored_str


def xor_with_bytes(message: bytes, key: bytes):
    return [(a ^ b) for a, b in zip(message, cycle(key))]


if __name__ == '__main__':
    message = 'b914 0645 71e0 b5f7 3707 cb85'
    key = 'FF D8 FF E0 00 10 4A 46 49 46 00 01'
    key_for_message = xor(message, key)
    print(key_for_message)
    key_for_message_bytes = bytes.fromhex(key_for_message.strip())
    with open('my_magic_bytes.jpg.enc', 'rb') as file:
        whole_message = file.read()
        decrypted = xor_with_bytes(whole_message, key_for_message_bytes)
        decrypted = bytes(decrypted)
        # print(decryped.hex())
        with open('out.jpg', 'wb') as outfile:
            outfile.write(decrypted)
