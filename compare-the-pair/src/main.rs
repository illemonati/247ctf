
use rayon::prelude::*;

const SALT: &str = "f789bbc328a3d1a3";


fn main() {
    (0..500000000).into_par_iter().for_each(|i| {
        let password = format!("{}{}", SALT, i);
        let password_bytes = password.as_bytes();
        let digest = md5::compute(password_bytes);
        let digest_str = format!("{:?}", digest);

        if &digest_str[0..2] == "0e" && digest_str[2..32].parse::<u128>().is_ok() {
            println!("{} {} {}", password, i, digest_str);
            std::process::exit(0);
        }
        if i % 100000 == 0 {
            println!("{}", i);
        }
    })
}
