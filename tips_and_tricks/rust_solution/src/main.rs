use regex::Regex;
use std::error::Error;
use std::io::BufReader;
use std::net::{TcpStream};
use std::io::{Read, Write, BufRead};
use std::str::from_utf8;

const CONN_STR: &str = "d1ea8e90917c744e.247ctf.com:50227";

fn main() -> Result<(), Box<dyn Error>> {
    let mut i = 0;
    let mut socket: std::net::TcpStream = TcpStream::connect(CONN_STR).expect("Socket error");
    let mut reader = BufReader::new(socket.try_clone().unwrap());
    let mut data_string = String::new();
    reader.read_line(&mut data_string)?;
    reader.read_line(&mut data_string)?;
    let re = Regex::new(r"(?m)\s(\d+)\s\+\s(\d+)")?;
    loop {
        let mut data_string = String::new();
        reader.read_line(&mut data_string)?;
        print!("{}) {}", i, data_string);
        let captures = re.captures(&data_string).unwrap();
        let a: usize = captures.get(1).map_or(0, |m| m.as_str().parse::<usize>().unwrap());
        let b: usize = captures.get(2).map_or(0, |m| m.as_str().parse::<usize>().unwrap());
        let sum = a + b;
        println!("{}", sum);
        socket.write(&format!("{}\r\n", sum).bytes().collect::<Vec<u8>>())?;
        let mut data_string = String::new();
        reader.read_line(&mut data_string)?;
        print!("{}", data_string);
        i += 1
    }
    
}